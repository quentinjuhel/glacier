# Glacier



Glacier est une famille de caractères typographique libre dessinée par  Quentin Juhel. Le dessin du caractère le Glacier se base sur le  squelette du Mercurius, tracé par Imre Reiner (graphiste, peintre et  dessinateur de caractère hongrois). Son dessin vernaculaire et son  expressivité rappelle le lettrage d'enseigne. Son nom fait référence à  l'expressivité des supports de communication des marchands de glaces  (enseignes, coupes en carton, menus, etc.).  

# Designer

Quentin Juhel 
[juhel-quentin.fr](https://juhel-quentin.fr)
[contact@juhel-quentin.fr](contact@juhel-quentin.fr)

# Licence

Glacier est distribuer sour licence SIL Open Font License v1.1 (http://scripts.sil.org/OFL)

Pour consulter les droits d'auteur et les conditions spécifiques, veuillez vous référer à  [OFL.txt](https://github.com/weiweihuanghuang/Work-Sans/blob/master/OFL.txt).

